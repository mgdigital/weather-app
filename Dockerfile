FROM node:10-alpine

USER root

RUN apk --no-cache add shadow sudo

RUN sed -e 's/# %wheel ALL=(ALL) NOPASSWD: ALL/%root ALL=(ALL) NOPASSWD: ALL/g' \
      -i /etc/sudoers

RUN usermod -a -G root node

ADD . /app/

RUN chown -R node:node /app/

USER node

WORKDIR /app/

RUN npm install

ENTRYPOINT /bin/sh

CMD ['-c', 'npm', 'start']

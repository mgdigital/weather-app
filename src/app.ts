import express from "express";
import * as api from "./api";

const app = express();

const getErrorCode = e => {
  if (typeof e === 'object') {
    if (e instanceof api.InvalidCityError) {
      return 404;
    }
    if (e instanceof api.ApiOptionsError) {
      return 400;
    }
  }
  return 500;
};

app.get('/:city', (req, res) =>
  api.fetchResult(Object.assign({}, req.query, {city: req.params.city.replace(/(_)/, ' ')}))
    .then(result => res.send(result))
    .catch(e => res.status(getErrorCode(e)).send(e.toString()))
);

export default app;

import request from 'request-promise-native';
import qs from 'querystring';

export type Location = {
  title: string,
  location_type: 'City' | 'Region / State / Province' | 'Country' | 'Continent',
  latt_long: string,
  woeid: number,
};

export type MetricMap = {
  weather_state_name: string,
  weather_state_abbr: string,
  wind_direction_compass: string,
  min_temp: number,
  max_temp: number,
  the_temp: number,
  wind_speed: number,
  wind_direction: number,
  air_pressure: number,
  humidity: number,
  visibility: number,
  predictability: number,
};

export type MetricType = keyof MetricMap;

export type MetricValue = string | number;

export type Weather = {
  id: number,
  applicable_date: string,
} & MetricMap;

export type LocationWeather = Location & {
  parent: Location,
  consolidated_weather: Weather[],
};

const baseUrl = 'https://www.metaweather.com/api/location/';

const createLocationSearchUri = (query: string) => baseUrl + 'search/?' + qs.stringify({query});

export const locationSearch = (query: string): Promise<Location[]> =>
  request.get(createLocationSearchUri(query)).then(JSON.parse);

const createLocationWeatherUri = (woeid: number) => baseUrl + woeid;

export const locationWeather = (woeid: number): Promise<LocationWeather> =>
  request.get(createLocationWeatherUri(woeid)).then(JSON.parse);

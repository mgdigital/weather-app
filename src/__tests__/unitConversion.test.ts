import { celsiusToFahrenheit, mphToKmph } from '../unitConversion';

it('converts from celsius to fahrenheit', () => {
  expect(celsiusToFahrenheit(0)).toEqual(32);
  expect(celsiusToFahrenheit(100)).toEqual(212);
  expect(celsiusToFahrenheit(-40)).toEqual(-40);
});

it('converts from mph to kmph', () => {
  expect(mphToKmph(0)).toEqual(0);
  expect(mphToKmph(1)).toEqual(1.60934);
  expect(mphToKmph(50)).toEqual(80.467);
});

import request from 'supertest';
const { ApiOptionsError, InvalidCityError } = require.requireActual('../api');

const apiMock = {
  fetchResult: jest.fn(() => new Promise((resolve, reject) => resolve({}))),
  ApiOptionsError,
  InvalidCityError,
};

jest.doMock('../api', () => apiMock);

const app = require('../app').default;

describe('express app', () => {

  it('calls the api fetchResult method with the city name', () =>
    request(app).get('/London')
      .then(() => {
        expect(apiMock.fetchResult).toHaveBeenCalledWith(expect.objectContaining({city: 'London'}));
      })
  );

  it('replaces underscores with spaces in the city name', () =>
    request(app).get('/new_york')
      .then(() => {
        expect(apiMock.fetchResult).toHaveBeenCalledWith(expect.objectContaining({city: 'new york'}));
      })
  );

  it('returns a 404 response when the city is invalid', () => {
    apiMock.fetchResult.mockImplementation(() => new Promise((resolve, reject) => {
      throw new InvalidCityError();
    }));
    return request(app).get('/invalid_city')
      .then(response => {
        expect(response.status).toEqual(404);
      });
  });

  it('returns a 400 response if invalid query parameters are supplied', () => {
    apiMock.fetchResult.mockImplementation(() => new Promise((resolve, reject) => {
      throw new ApiOptionsError();
    }));
    return request(app).get('/london?temp_format=X')
      .then(response => {
        expect(response.status).toEqual(400);
      });
  });

  it('returns a 500 response if any other error occurs', () => {
    apiMock.fetchResult.mockImplementation(() => new Promise((resolve, reject) => {
      throw new Error();
    }));
    return request(app).get('/london')
      .then(response => {
        expect(response.status).toEqual(500);
      });
  });

});

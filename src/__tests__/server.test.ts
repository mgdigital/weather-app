const mockApp = {
  listen: jest.fn(),
};

jest.doMock('../app', () => mockApp);

it('listens on port 3000', () => {
  require('../server');
  expect(mockApp.listen).toHaveBeenCalledWith(3000, expect.anything());
});

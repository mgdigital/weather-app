const cities = [
  {
    title: "London",
    location_type: "City",
    woeid: 44418,
    latt_long: "51.506321,-0.12714"
  },
  {
    title: "Paris",
    location_type: "City",
    woeid: 615702,
    latt_long: "48.856930,2.341200"
  },
];

const metaWeatherMock = {
  locationSearch: jest.fn((query: string) => new Promise((resolve, reject) =>
    resolve(cities.filter(c => c.title.toUpperCase() === query.toUpperCase()))
  )),
  locationWeather: jest.fn((woeid: number) => new Promise((resolve, reject) => {
    const location = cities.filter(c => c.woeid === woeid)[0];
    expect(location).toHaveProperty('title');
    resolve(Object.assign({}, location, {
      consolidated_weather: [
        {
          applicable_date: '2018-09-09',
          weather_state_name: 'TEST',
          weather_state_abbr: 'TEST',
          wind_direction_compass: 'NW',
          min_temp: 0,
          max_temp: 10,
          the_temp: 5,
          wind_speed: 5,
          wind_direction: 200,
          air_pressure: 1000,
          humidity: 50,
          visibility: 10,
          predictability: 50,
        }
      ]
    }));
  })),
};

jest.doMock('../metaWeather', () => metaWeatherMock);

const api = require('../api');

describe('api', () => {

  it('returns result for a city', () =>
    api.fetchResult({city: 'London'})
      .then(result => {
        expect(metaWeatherMock.locationSearch).toHaveBeenCalledWith('London');
        expect(metaWeatherMock.locationWeather).toHaveBeenCalledWith(44418);
        expect(result.city).toEqual('London');
        expect(result.days.length).toBeGreaterThanOrEqual(1);
        expect(result.days[0]).toHaveProperty('metrics');
      })
  );

});

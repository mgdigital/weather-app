import * as mw from './metaWeather';
import { celsiusToFahrenheit, mphToKmph } from './unitConversion';

type TempFormat = 'C' | 'F';
type WindSpeedFormat = 'M' | 'K';

export type Options = {
  city: string,
  short?: boolean,
  all?: boolean,
  temp_format?: TempFormat,
  wind_speed_format?: WindSpeedFormat,
  number_of_days?: number,
  metrics?: mw.MetricType[],
};

export type Result = {
  city: string,
  days: {
    date: string,
    metrics: {[type in mw.MetricType]: mw.MetricValue},
  }[],
  temp_format: TempFormat,
  wind_speed_format: WindSpeedFormat,
};

export class ApiOptionsError extends Error {}

export class InvalidCityError extends ApiOptionsError {
  constructor(message = 'No matching city found') {
    super(message);
  }
}

export class InvalidTempFormatError extends ApiOptionsError {
  constructor(message = 'Invalid temperature format') {
    super(message);
  }
}

export class InvalidWindSpeedFormatError extends ApiOptionsError {
  constructor(message = 'Invalid wind speed format') {
    super(message);
  }
}

const metricTypeSets: {[name: string]: mw.MetricType[]} = {
  all: [
    'weather_state_name',
    'weather_state_abbr',
    'wind_direction_compass',
    'min_temp',
    'max_temp',
    'the_temp',
    'wind_speed',
    'wind_direction',
    'air_pressure',
    'humidity',
    'visibility',
    'predictability',
  ],
  default: [
    'wind_speed',
    'wind_direction_compass',
    'min_temp',
    'max_temp',
    'the_temp',
    'humidity',
    'visibility',
    'air_pressure',
  ],
  short: [
    'weather_state_name',
    'min_temp',
    'max_temp',
    'the_temp',
  ],
  temperatures: [
    'min_temp',
    'max_temp',
    'the_temp',
  ]
};

const getMetricTypes = (options: Options): mw.MetricType[] => {
  let types;
  if (options.short) {
    types = metricTypeSets.short;
  } else if (options.all || options.metrics) {
    types = metricTypeSets.all;
  } else {
    types = metricTypeSets.default;
  }
  if (options.metrics) {
    types = types.filter(t => options.metrics.indexOf(t) >= 0);
  }
  return types;
};

const getTempFormat = (options: Options): TempFormat => {
  switch (options.temp_format ? options.temp_format : '') {
    case 'C':
    case '':
      return 'C';
    case 'F':
      return 'F';
    default:
      throw new InvalidTempFormatError();
  }
};

const getWindSpeedFormat = (options: Options): WindSpeedFormat => {
  switch (options.wind_speed_format ? options.wind_speed_format : '') {
    case 'M':
    case '':
      return 'M';
    case 'K':
      return 'K';
    default:
      throw new InvalidWindSpeedFormatError();
  }
};

const valueToNumber = (value: mw.MetricValue): number => typeof value === 'number' ? value : parseFloat(value);

const transformMetric = (type: mw.MetricType, value: mw.MetricValue, options: Options): mw.MetricValue => {
  if (metricTypeSets.temperatures.indexOf(type) >= 0 && getTempFormat(options) === 'F') {
    return celsiusToFahrenheit(valueToNumber(value));
  }
  if (type === 'wind_speed' && getWindSpeedFormat(options) === 'K') {
    return mphToKmph(valueToNumber(value));
  }
  return value;
};

const transformLocationWeather = (locationWeather: mw.LocationWeather, options: Options): Result => {
  const metricTypes = getMetricTypes(options);
  return {
    city: locationWeather.title,
    days: locationWeather.consolidated_weather
      .slice(0, options.number_of_days)
      .map(weather => ({
        date: weather.applicable_date,
        metrics: metricTypes.reduce(
          (metrics, metricType) => Object.assign(
            metrics,
            weather.hasOwnProperty(metricType) ? {[metricType]: transformMetric(metricType, weather[metricType], options)} : {}
          ),
          {} as {[type in mw.MetricType]: mw.MetricValue}
        ),
      })),
    temp_format: getTempFormat(options),
    wind_speed_format: getWindSpeedFormat(options),
  };
};

export const fetchResult = (options: Options): Promise<Result> =>
  mw.locationSearch(options.city)
    .then(locations => locations.filter(l => l.location_type === 'City'))
    .then(locations => {
      if (locations.length < 1) {
        throw new InvalidCityError();
      }
      return locations[0];
    })
    .then(location => mw.locationWeather(location.woeid))
    .then(lw => transformLocationWeather(lw, options));

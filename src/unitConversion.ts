export const celsiusToFahrenheit = (celsius: number) => celsius * 1.8 + 32;

export const mphToKmph = (mph: number) => mph * 1.60934;

import fs from 'fs';
import junit from 'cucumber-junit';

const json = fs.readFileSync('./tmp/cucumber.json').toString();
const xml = junit(json, { indent: '    ', strict: true });
fs.writeFileSync('./junit/junit-cucumber.xml', xml);

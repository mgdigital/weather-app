import 'jest';
import { Before, Given, When, Then } from 'cucumber';
import request from 'supertest';
import qs from 'querystring';
import app from '../../src/app';
const expect = require('expect');

type State = {
  chosenCity?: string,
  params: {[key: string]: string},
};

const createInitialState = (): State => ({
  params: {},
});

const doRequest = (state: State) => {
  const city = state.chosenCity;
  expect(city).toMatch(/.+/);
  const url = '/' + encodeURI(city) + (Object.keys(state.params).length ? '?' + qs.stringify(state.params) : '');
  return request(app).get(url);
};

const doFetchResponse = (state: State) =>
  doRequest(state)
    .expect(200)
    .then(response => JSON.parse(response.text));

const setCity = (state: State, city: string) => {
  Object.assign(state, {chosenCity: city});
};

const setParam = (state: State, key: string, value) => {
  Object.assign(state.params, {[key]: value});
};

Before(() => {
  this.state = createInitialState();
});

Given(
  'I want to search for the weather for a particular city',
  () => {}
);

When(
  'I provide the city {string}',
  (city: string) => setCity(this.state, city)
);

Then(
  'I receive a response with the weather details for the requested city',
  () => doFetchResponse(this.state)
    .then(result => {
      expect(result.city).toEqual(this.state.chosenCity);
      expect(result.days.length).toBeGreaterThanOrEqual(1);
      expect(result.days[0].metrics).toHaveProperty('wind_speed');
    })
);

Then(
  'I receive a {int} response with the message {string}',
  (responseCode: number, message: string) => doRequest(this.state)
      .expect(responseCode)
      .then(response => expect(response.text).toContain(message))
);

Given(
  '{string} is provided in the request', 
  (key: string) => setParam(this.state, key, true)
);

Given(
  '{string} is provided in the request and it is {string}',
  (key: string, value: string) => setParam(this.state, key, value)
);

Given(
  '{string} is provided in the request and it is {int}',
  (key: string, value: number) => setParam(this.state, key, value)
);

Given(
  'forecast metrics {string} are provided in the request',
  (metrics: string) => setParam(this.state, 'metrics', metrics.split(/\s*,\s*/))
);

Then(
  'the only metrics returned should be:',
  table => doFetchResponse(this.state)
    .then(result => {
      const expectedKeys = table.raw().map(row => row[0]).reduce((all, cell) => all.concat(cell.split(/\s*,\s*/)), []);
      expect(result.days.length).toBeGreaterThanOrEqual(1);
      for (const i in result.days) {
        let returnedMetrics = Object.keys(result.days[i].metrics);
        expect(returnedMetrics).toHaveLength(expectedKeys.length);
        expect(returnedMetrics).toEqual(expect.arrayContaining(returnedMetrics));
      }
    })
);

Then(
  'weather metrics for {int} days must be returned',
  (dayCount: number) => doFetchResponse(this.state)
    .then(result => expect(result.days).toHaveLength(dayCount))
);

Then(
  'the temperature format for all temperatures in the forecast must be {string}', 
  format => doFetchResponse(this.state)
    .then(result => expect(result.temp_format).toEqual(format))
);

Then(
  'the wind speed format for all wind speeds in the forecast must be {string}',
  format => doFetchResponse(this.state)
    .then(result => expect(result.wind_speed_format).toEqual(format))
);

Feature: Users need to be able to request short-form weather or particular forecast metrics

  Background:
    Given I want to search for the weather for a particular city
    And I provide the city "London"

  Scenario: Specify short-term weather request
    Given "short" is provided in the request
    Then the only metrics returned should be:
      | weather_state_name |
      | min_temp           |
      | max_temp           |
      | the_temp           |

  Scenario Outline: Specify number of days
    Given "number_of_days" is provided in the request and it is <number_of_days>
    Then weather metrics for <number_of_days> days must be returned

    Examples:
      | number_of_days |
      | 1              |
      | 3              |
      | 6              |

  Scenario Outline: Specify forecast metric
    Given forecast metrics "<forecast_metrics>" are provided in the request
    Then the only metrics returned should be:
      | <forecast_metrics> |

    Examples:
      | forecast_metrics                                                         |
      | weather_state_name, weather_state_abbr, wind_direction_compass, min_temp |
      | max_temp, the_temp, wind_speed                                           | 
      | wind_direction, air_pressure, humidity, visibility, predictability       |

  Scenario Outline: Specify temperature format
    Given "temp_format" is provided in the request and it is "<temp_format>"
    Then the temperature format for all temperatures in the forecast must be "<temp_format>"

    Examples:
      | temp_format |
      | C           |
      | F           |

  Scenario: Specify invalid temperature format
    Given "temp_format" is provided in the request and it is "X"
    Then I receive a 400 response with the message "Invalid temperature format"

  Scenario Outline: Specify wind speed format
    Given "wind_speed_format" is provided in the request and it is "<wind_speed_format>"
    Then the wind speed format for all wind speeds in the forecast must be "<wind_speed_format>"

    Examples:
      | wind_speed_format |
      | K                 |
      | M                 |

  Scenario: Specify invalid wind speed format
    Given "wind_speed_format" is provided in the request and it is "X"
    Then I receive a 400 response with the message "Invalid wind speed format"

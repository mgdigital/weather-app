Feature: Users need to be able to search for weather for a city

  Background:
    Given I want to search for the weather for a particular city

  Scenario Outline: Search for weather for a valid city
    When I provide the city "<city>"
    Then I receive a response with the weather details for the requested city

    Examples:
      | city     |
      | London   |
      | New York |

  Scenario: Search for weather for an invalid city
    When I provide the city "Nodnol"
    Then I receive a 404 response with the message "No matching city found"

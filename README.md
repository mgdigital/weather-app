# NodeJS Weather API

This application provides a simple weather API.

## Instructions

### Installation

Install the app by running `npm install`.

### Starting the API server

The API server can be started by running `npm start`. It will listen on port 3000.

### Making requests to the API

The API accepts GET requests. The path should be equal to the desired city. For convenience, underscores are replaced with spaces for multi-word cities.

The following options can be passed as query parameters:

- `short=1`: Will return result with the metrics `weather_state_name`, `min_temp`, `max_temp` and `the_temp`
- `all=1`: Will return all available metrics (as specified the default list of metrics returned does not include all those available)
- `temp_format`: Will return result with all temperatures in either `C` (celsius) or `F` (fahrenheit)
- `wind_speed_format`: Will return all wind speeds in either `M` (mph) or `K` (kmph)
- `number_of_days`: Will limit the number of days forecast to the specified integer
- `metrics`: Will return result with the list of metrics provided

The names of available metrics reflect those provided by the MetaWeather API. The full list of these is as follows. (d) indicates that the metric is returned by default.

- `weather_state_name`
- `weather_state_abbr`
- `wind_direction_compass` (d)
- `min_temp` (d)
- `max_temp` (d)
- `the_temp` (d)
- `wind_speed` (d)
- `wind_direction`
- `air_pressure` (d)
- `humidity` (d)
- `visibility` (d)
- `predictability`

#### Example requests

```
GET /london
GET /New%20York
GET /new_york?short=1
GET /Paris?temp_format=F
GET /paris?wind_speed_format=K
GET /london?metrics[]=min_temp&metrics[]=wind_direction
```

### Running tests

- Unit tests (using Jest) can be run with `npm run-script jest`.
- Acceptance tests (using Cucumber) can be run with `npm run-script cucumber`.
- Code linting can be run with `npm run-script tslint`.
- By running `npm test`, linting, Jest and Cucumber tests will all be executed.
- Separate coverage reports are provided for unit and acceptance tests.
